# CI/CD components for MkDocs

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for an [MkDocs](https://www.mkdocs.org/) project.
